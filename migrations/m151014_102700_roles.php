<?php

use yii\db\Schema;
use yii\db\Migration;

class m151014_102700_roles extends Migration
{
    public function up()
    {
	    $roleAdmin = \Yii::$app->authManager->createRole('admin');
	    $roleAdmin->description = 'Админ';
	    \Yii::$app->authManager->add($roleAdmin);


	    $roleUser = \Yii::$app->authManager->createRole('user');
	    $roleUser->description = 'Пользователь';
	    \Yii::$app->authManager->add($roleUser);

	    \Yii::$app->authManager->addChild($roleAdmin, $roleUser);
    }

    public function down()
    {
        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
