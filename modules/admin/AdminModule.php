<?php

namespace app\modules\admin;

class AdminModule extends \yii\base\Module
{
	public $defaultRoute = 'books';
	public $layout = 'main';
    public $controllerNamespace = 'app\modules\admin\controllers';

	public function beforeAction($action) {
		if (!\Yii::$app->user->can('admin')) {
			\Yii::$app->getResponse()->redirect(\Yii::$app->homeUrl, 403);
			return false;
		}

		return parent::beforeAction($action);
	}
}
