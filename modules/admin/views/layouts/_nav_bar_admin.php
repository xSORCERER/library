<?php
/*Created by Edik (14.10.2015 14:38)*/
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>

<?php
    NavBar::begin([
        'options' => [
            'class' => 'navbar-default navbar-relative-top navbar-admin',
        ],
    ]);
?>

    <?=
        Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
	            ['label' => 'Список книг', 'url' => ['/admin/books'], 'active' => \Yii::$app->controller->id == 'books'],
	            ['label' => 'Авторы', 'url' => ['/admin/authors'], 'active' => \Yii::$app->controller->id == 'authors'],
	            ['label' => 'Пользователи', 'url' => ['/admin/users'], 'active' => \Yii::$app->controller->id == 'users'],
	            ['label' => 'История', 'url' => ['/admin/history'], 'active' => \Yii::$app->controller->id == 'history']
            ],
        ]);
    ?>

<?php
    NavBar::end();
?>