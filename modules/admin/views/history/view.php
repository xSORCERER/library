<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BookUsersHistory */

$this->title = $model->id_book;
$this->params['breadcrumbs'][] = ['label' => 'Book Users Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-users-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_book' => $model->id_book, 'id_user' => $model->id_user], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_book' => $model->id_book, 'id_user' => $model->id_user], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_book',
            'id_user',
            'date_add',
            'date_return',
        ],
    ]) ?>

</div>
