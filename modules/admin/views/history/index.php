<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История';
?>
<div class="book-users-history-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => 'Показано {begin}-{end} из {totalCount}',
        'emptyText' => 'По вашему запросу ничего не найдено',
        'columns' => [
            [
	            'label' => 'Книга',
				'value' => 'book.name'
            ],
            [
                'label' => 'Пользователь',
                'value' => 'user.firstname'
            ],
            'date_add',
            'date_return',

            ['class' => 'yii\grid\ActionColumn', 'template'=>'{delete}'],
        ],
    ]); ?>

</div>
