<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список книг';
?>
<div class="books-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить книгу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => 'Показано {begin}-{end} из {totalCount}',
        'emptyText' => 'По вашему запросу ничего не найдено',
        'columns' => [
            [
	            'attribute' => 'id_book',
	            'options' => ['style' => 'width: 70px;'],
            ],
            [
	            'attribute' => 'id_status',
	            'value' => function($model){
		            $value = $model->getStatusName();
		            if ($model->id_status == 2) $value .= ' (' . $model->bookUser->firstname . ')';
		            return $value;
	            },
	            'filter' => \app\models\BookStatuses::findAllList()
            ],
            'name',
            [
	            'label' => 'Автор',
                'value' => function($model){
                    $value = [];
	                foreach($model->bookAuthors as $author) $value[] = $author->lastname;
                    return implode(', ', $value);
                }
            ],
            'description:ntext',

            ['class' => 'yii\grid\ActionColumn', 'contentOptions' => ['style' => 'white-space: nowrap;']],
        ],
    ]); ?>

</div>
