<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Books */

$this->title = $model->name;
?>
<div class="books-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id_book], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id_book], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

	<?php
		$attributes = [
            'id_book',
            [
                'attribute' => 'id_status',
                'value' => $model->getStatusName()
            ],
            'name',
            'description:ntext'
		];
		if ((int)$model->id_status === 2 && $model->bookUser) {
			//если книга выдана пользователю, то добавляем еще 1 строчку
			$attributes[] = [
                'label' => 'Книга выдана пользователю',
                'value' => $model->bookUser->firstname . ' ' . $model->bookUser->lastname . ' ' . $model->bookUser->patronymic
            ];
		}
	?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>
