<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\AuthorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Авторы';
?>
<div class="authors-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить автора', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => 'Показано {begin}-{end} из {totalCount}',
        'emptyText' => 'По вашему запросу ничего не найдено',
        'columns' => [
	        [
                'attribute' => 'id_author',
                'options' => ['style' => 'width: 70px;'],
            ],
            'firstname',
            'lastname',
            'patronymic',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
