<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => 'Показано {begin}-{end} из {totalCount}',
        'emptyText' => 'По вашему запросу ничего не найдено',
        'columns' => [
	        [
                'attribute' => 'id_user',
                'options' => ['style' => 'width: 70px;'],
            ],
            'email:email',
            'firstname',
            'lastname',
            'patronymic',
            [
	            'label' => 'Роль',
                'value' => function($model){
	                $roles = \Yii::$app->authManager->getRolesByUser($model->id_user);
	                return current($roles)->description;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
