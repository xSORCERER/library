<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = 'Информация о пользователе ' . $model->id_user;
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id_user], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id_user], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

	<?php $roles = \Yii::$app->authManager->getRolesByUser($model->id_user) ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_user',
            'email:email',
            'firstname',
            'lastname',
            'patronymic',
            [
                'label' => 'Роль',
                'value' => current($roles)->description
            ]
        ],
    ]) ?>

</div>
