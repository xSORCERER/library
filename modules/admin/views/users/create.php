<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Добавить пользователя';
?>
<div class="users-create">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="users-form">

	    <?php $form = ActiveForm::begin(); ?>

		<?= $form->errorSummary($model); ?>

	    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'passwordRepeat')->passwordInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

	    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'role')->dropDownList(ArrayHelper::map(Yii::$app->authManager->roles, 'name', 'description')) ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

	    <?php ActiveForm::end(); ?>

	</div>


</div>
