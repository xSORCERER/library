<?php

namespace app\modules\admin\models;
use app\models\Authors;
use app\models\BookAuthors;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

/**
 * This is the model class for table "books".
 *
 * @property string $id_book
 * @property integer $id_status
 * @property string $name
 * @property string $description
 *
 * @property BookAuthors[] $bookAuthors
 * @property Authors[] $idAuthors
 * @property BookUsers[] $bookUsers
 * @property Users[] $idUsers
 * @property BookStatuses $bookStatus
 */
class Books extends \app\models\Books
{
	public $idsAuthors = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
	    $rules = parent::rules();
	    $rules[] = ['idsAuthors', 'required'];
	    $rules[] = ['idsAuthors', 'checkIdsAuthors'];
	    return $rules;
    }

	public function checkIdsAuthors($key){
		if (!is_array($this->$key)) $this->addError($key, 'Не верно заполнен ' . $this->attributeLabels()[$key]);

		//убираем повторяющиеся
		$this->$key = array_unique($this->$key);
		//убираем корявые ids (если передали не то)
		$this->$key = array_intersect($this->$key, array_keys(Authors::findAllList()));

		if (!$this->$key) $this->addError($key, 'Не верно заполнен ' . $this->attributeLabels()[$key]);
	}

	public function afterFind(){
		//при редактировании добавляем авторов книги
		if (!$this->isNewRecord){
			$this->idsAuthors = BookAuthors::find()->select('id_author')->where(['id_book' => $this->id_book])->asArray()->all();
			$this->idsAuthors = ArrayHelper::getColumn($this->idsAuthors, 'id_author');
		}
		parent::afterFind();
	}

	public function afterSave($insert, $changedAttributes) {
		//----
		//сохраняем авторов книги
		$db = \Yii::$app->db;
		if (!$insert) {
			BookAuthors::deleteAll('id_book = :id_book', [':id_book' => $this->id_book]);
		}
		$insertValues = [];
		foreach ($this->idsAuthors as $idAuthor) {
		    $insertValues[] = [$this->id_book, $idAuthor];
		}

		$db->createCommand(
			$db->getQueryBuilder()->batchInsert(BookAuthors::tableName(), ['id_book', 'id_author'], $insertValues)
		)->query();
		//----
		//----
		parent::afterSave($insert, $changedAttributes);
	}

	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
	    $labels = parent::attributeLabels();
	    $labels['idsAuthors'] = 'Авторы';
	    return $labels;
    }

}
