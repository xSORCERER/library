<?php
/*Created by Edik (14.10.2015 21:59)*/
namespace app\modules\admin\models;

use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

class UsersUpdate extends \app\models\Users{
	public $role = null;

	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['role'] = 'Роль';
		return $labels;
	}

	public function rules() {
		$rules = parent::rules();
		$rules[] = ['role', 'in', 'range' => ArrayHelper::getColumn(\Yii::$app->authManager->roles, 'name')];
		return $rules;
	}

	public function afterFind() {
		if ($this->role === null){
			//добавляем роль(что бы в форме показалась нужная)
			$this->role = \Yii::$app->authManager->getRolesByUser($this->id_user);
			$this->role = array_keys($this->role)[0];
		}
		parent::afterFind();
	}

	public function afterSave($insert, $changedAttributes) {
		if ($insert !== true){
			//заменяем предыдущую роль новой
			$userRole = \Yii::$app->authManager->getRole($this->role);
			\Yii::$app->authManager->revokeAll($this->id_user);
			\Yii::$app->authManager->assign($userRole, $this->id_user);

			//чистим кэш этого пользователя
			TagDependency::invalidate(\Yii::$app->cache, self::tableName() . '-' . $this->id_user);
		}
		parent::afterSave($insert, $changedAttributes);
	}
}