<?php
/*Created by Edik (14.10.2015 21:59)*/
namespace app\modules\admin\models;

use app\models\User;
use yii\helpers\ArrayHelper;

class UsersReg extends \app\models\UsersReg{
	public $isNewRecord = true, $role = 'user', $id_user;

	public function attributeLabels() {
		$labels = parent::attributeLabels();
		$labels['role'] = 'Роль';
		return $labels;
	}


	public function rules() {
		$rules = parent::rules();
		$rules[] = ['role', 'in', 'range' => ArrayHelper::getColumn(\Yii::$app->authManager->roles, 'name')];
		return $rules;
	}

	/**
	 * Logs in a user using the provided email and password.
	 * @return boolean whether the user is logged in successfully
	 */
	public function registration() {
		if ($this->validate()){
			$user = new User();
			if ($user->load($this->getAttributes(), '') && $user->save(false)){
				$this->id_user = $user->getId();

				$userRole = \Yii::$app->authManager->getRole($this->role);
				\Yii::$app->authManager->assign($userRole, $this->id_user);

				return true;
			}
		}
		return false;
	}
}