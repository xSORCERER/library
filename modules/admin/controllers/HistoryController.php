<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\BookUsersHistory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HistoryController implements the CRUD actions for History model.
 */
class HistoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BookUsersHistory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => BookUsersHistory::find()->with(['book', 'user']),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing BookUsersHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id_book
     * @param string $id_user
     * @return mixed
     */
    public function actionDelete($id_book, $id_user)
    {
        $this->findModel($id_book, $id_user)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BookUsersHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id_book
     * @param string $id_user
     * @return BookUsersHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_book, $id_user)
    {
        if (($model = BookUsersHistory::findOne(['id_book' => $id_book, 'id_user' => $id_user])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
