<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

class BooksSearchHistory
{

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = BookUsersHistory::find()
	        ->select(['id_book', 'id_user', 'date_add', 'date_return'])
	        ->with([
		        'user' => function(\yii\db\ActiveQuery $relation){
			        $relation->select(['id_user', 'firstname']);
		        },
	            'book' => function(\yii\db\ActiveQuery $relation){
		            $relation->select(['id_book', 'name']);
	            }
	        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

	    return $dataProvider;
    }
}
