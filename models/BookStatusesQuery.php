<?php

namespace app\models;

use app\components\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[BookStatuses]].
 *
 * @see BookStatuses
 */
class BookStatusesQuery extends ActiveQuery
{

    /**
     * @inheritdoc
     * @return BookStatuses[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BookStatuses|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}