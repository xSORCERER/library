<?php

namespace app\models;

/**
 * This is the model class for table "books".
 *
 * @property string $id_book
 * @property integer $id_status
 * @property string $name
 * @property string $description
 *
 * @property BookAuthors[] $bookAuthors
 * @property Authors[] $idAuthors
 * @property BookUsers[] $bookUsers
 * @property Users[] $idUsers
 * @property BookStatuses $bookStatus
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['id_status'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

	/**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
	        'id_book' => 'Id Book',
            'id_status' => 'Статус',
            'name' => 'Наименование',
            'description' => 'Описание'
        ];
    }

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getStatusName()
	{
		return BookStatuses::findAllList()[$this->id_status];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookAuthors()
    {
        return $this->hasMany(Authors::className(), ['id_author' => 'id_author'])->viaTable('book_authors', ['id_book' => 'id_book']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookUser()
    {
        return $this->hasOne(Users::className(), ['id_user' => 'id_user'])->viaTable('book_users', ['id_book' => 'id_book']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(BookUsers::className(), ['id_user' => 'id_user']);
	}
}
