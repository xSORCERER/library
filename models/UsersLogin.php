<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class UsersLogin extends Model{
    public $email, $password, $rememberMe = true;
    private $_user = false;

	public function rules() {
		return [
			// email rules
			'emailRequired' => ['email', 'required'],
			'emailTrim' => ['email', 'trim'],
			'emailMatch' => ['email', 'email'],

			// password rules
			'passwordRequired' => ['password', 'required'],
			'passwordLength' => ['password', 'string', 'min' => 4],
			'passwordValidate' => ['password', 'validatePassword'],

			'rememberMe' => ['rememberMe', 'boolean'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'email' => 'Email',
			'password' => 'Пароль'
		];
	}

    /**Проверка пароля и email.
     * @param string $attribute
     */
    public function validatePassword($attribute){
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильный логин или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser(){
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}