<?php
namespace app\models;

use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**Класс нужен что бы показать все книги, которые в данный момент выданы*/
class BooksSearchStatistic
{

    /**
     * Creates data provider instance with search query applied
     * @return ArrayDataProvider
     */
    public function search()
    {
	    $statistic = [];

	    //колчисество книг в библиотеке\отданных книг
	    $query = (new Query())
		    ->select(['count' => 'count(*)', 'id_status'])
		    ->from(Books::tableName())
		    ->groupBy('id_status')
		    ->createCommand()
		    ->queryAll();

	    $statistic['countBooks'] = ArrayHelper::map($query, 'id_status', 'count');

	    //колчисество должников
        $query = (new Query())
            ->select(['count' => 'count(distinct id_user)'])
            ->from(BookUsers::tableName())
            ->createCommand()
            ->queryScalar();

	    $statistic['countUsers'] = $query;

	    return $statistic;
    }
}
