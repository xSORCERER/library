<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $id_user
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $firstname
 * @property string $lastname
 * @property string $patronymic
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'firstname'], 'required'],
            ['email', 'email'],
            ['email', 'trim'],
            ['email', 'string', 'max' => 127],
            [['password_hash', 'auth_key'], 'string'],
            [['auth_key'], 'string', 'max' => 64],
            [['firstname', 'lastname', 'patronymic'], 'string', 'max' => 127],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'patronymic' => 'Отчество',
        ];
    }
}
