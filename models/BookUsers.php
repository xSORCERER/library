<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book_users".
 *
 * @property string $id_book
 * @property string $id_user
 * @property string $date_add
 */
class BookUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_book', 'id_user'], 'required'],
            [['id_book', 'id_user'], 'integer'],
            [['date_add'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_book' => 'Id Book',
            'id_user' => 'Id User',
            'date_add' => 'Date Add',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(Users::className(), ['id_user' => 'id_user']);
	}
}
