<?php
namespace app\models;

use \yii\base\Model;

/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property integer $id_city
 * @property string $firstname
 * @property string $lastname
 * @property string $patronomic
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property string $accessToken
 */
class UsersReg extends Users {
	public $password, $passwordRepeat, $rememberMe = true;

	public function rules() {
		$rules = parent::rules();
		return array_merge($rules, [
			'rememberMe' => ['rememberMe', 'boolean'],

			// email rules
			'emailUnique' => ['email', function($key) {
				if (User::findByEmail($this->$key)){
					$this->addError($key, 'Указанный Email уже существует');
				}
			}],

			// password rules
			'passwordRequired' => ['password', 'required'],
			'passwordLength' => ['password', 'string', 'min' => 4],

			'passwordRepeatRequired' => ['passwordRepeat', 'required'],
			'passwordRepeat' => ['passwordRepeat', function($key) {
				if ($this->$key !== $this->password){
					$this->addError($key, 'Пароли не совпадают');
				}
			}]
		]);
	}


	public function getAttributes($names = null, $except = []) {
		$attributes = parent::getAttributes($names, $except);

		$attributes['password'] = $this->password;
		return $attributes;
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		$labels = parent::attributeLabels();
		return array_merge($labels, [
			'password' => 'Пароль',
			'passwordRepeat' => 'Еще раз пароль'
		]);
	}

	/**
	 * Logs in a user using the provided email and password.
	 * @return boolean whether the user is logged in successfully
	 */
	public function registration() {
		if ($this->validate()){
			$user = new User();
			if ($user->load($this->getAttributes(), '') && $user->save(false)){

				$userRole = \Yii::$app->authManager->getRole('user');
				\Yii::$app->authManager->assign($userRole, $user->getId());

				return \Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
			}
		}
		return false;
	}
}
