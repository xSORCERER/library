<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BooksSearch represents the model behind the search form about `app\models\Books`.
 */
class BooksSearch extends Books
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_book', 'id_status'], 'integer'],
            [['name', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Books::find()->select(['id_book', 'name', 'id_status', 'description'])
	        ->with([
		        'bookUser' => function(\yii\db\ActiveQuery $relation){
			        $relation->select(['id_user', 'firstname']);
		        },
		        'bookAuthors' => function(\yii\db\ActiveQuery $relation){
                    $relation->select(['id_author', 'firstname', 'lastname', 'patronymic']);
                }
	        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
	            'pageSize' => 8
            ]
        ]);
	    return $dataProvider;
    }
}
