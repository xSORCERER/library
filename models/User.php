<?php
namespace app\models;

use yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {
	public $password;

	public function rules() {
		return [
			[['firstname', 'lastname', 'patronymic', 'password', 'email'], 'string']
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id){
	    return self::find()->cache(self::tableName() . '-' . $id)->where(['id_user' => $id])->one();
    }

    public static function findIdentityByAccessToken($token, $type = null){
	    return false;
    }

    /**
     * Finds user by email
     *
     * @param  string $email
     * @return static|null
     */
    public static function findByEmail($email){
	    $key = 'user-find-by-email-' . $email;
	    $user = yii::$app->cache->get($key);
	    if ($user === false){
		    $user = self::find()->where(['email' => $email])->one();
		    if ($user) yii::$app->cache->setWithDependency($key, $user, [self::tableName(), self::tableName() . '-' . $user['id_user']]);
	    }
	    return $user;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id_user;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey(){
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey){
        return $this->auth_key === $authKey;
    }

    /**Проверка пароля из формы авторизации
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return md5('pwHash' . $password . 'library') === $this->password_hash;
    }

	public function beforeSave($insert) {
		if ($insert){
			$this->auth_key = yii::$app->security->generateRandomString();
			if ($this->password) $this->password_hash = md5('pwHash' . $this->password . 'library');
		}
		return parent::beforeSave($insert);
	}

	public static function tableName(){
        return 'users';
    }

	public static function find(){
		return new UsersQuery(get_called_class());
	}
}
