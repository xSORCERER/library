<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book_users_history".
 *
 * @property string $id_book
 * @property string $id_user
 * @property string $date_add
 * @property string $date_return
 */
class BookUsersHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_users_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_book', 'id_user'], 'required'],
            [['id_book', 'id_user'], 'integer'],
            [['date_add', 'date_return'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_book' => 'Id Book',
            'id_user' => 'Id User',
            'date_add' => 'Дата выдачи',
            'date_return' => 'Дата возврата',
        ];
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(Users::className(), ['id_user' => 'id_user']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBook() {
		return $this->hasOne(Books::className(), ['id_book' => 'id_book']);
	}
}
