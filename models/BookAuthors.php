<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "book_authors".
 *
 * @property string $id_book
 * @property string $id_author
 */
class BookAuthors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_book', 'id_author'], 'required'],
            [['id_book', 'id_author'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_book' => 'Id Book',
            'id_author' => 'Id Author',
        ];
    }
}
