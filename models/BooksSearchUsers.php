<?php
namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**Класс нужен что бы показать все книги, которые в данный момент выданы*/
class BooksSearchUsers
{

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search()
    {
	    $query = new Query();
	    $query->select(['t.id_book', 't.name', 't.id_status', 'tU.firstname', 'tBU.date_add', 'tBU.id_user'])
		      ->from(['t' =>Books::tableName()])
		      ->innerJoin(['tBU' => BookUsers::tableName()], 't.id_book = tBU.id_book')
		      ->innerJoin(['tU' => Users::tableName()], 'tBU.id_user = tU.id_user')
		      ->where('t.id_status = 2');

	    //не админы видят только свои книги
	    if (!Yii::$app->user->can('admin')){
		    $query->andWhere('tBU.id_user = ' . Yii::$app->user->id);
	    }

        $dataProvider = new ActiveDataProvider([
	        'query' => $query
        ]);

	    return $dataProvider;
    }
}
