<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "authors".
 *
 * @property string $id_author
 * @property string $firstname
 * @property string $lastname
 * @property string $patronymic
 */
class Authors extends \yii\db\ActiveRecord
{
	private static $listAuthors = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'patronymic'], 'required'],
            [['firstname', 'lastname', 'patronymic'], 'string', 'max' => 127]
        ];
    }

	public static function findAllList(){
		if (self::$listAuthors === null){
			$key = 'all-autors-list';
			self::$listAuthors = Yii::$app->cache->get($key);
			if (self::$listAuthors === false){
				self::$listAuthors = self::find()->select(['id_author', 'concat(firstname, " ", lastname, " ", patronymic) as name'])->asArray()->all();

				if (self::$listAuthors) {
					self::$listAuthors = ArrayHelper::map(self::$listAuthors, 'id_author', 'name');
					yii::$app->cache->setWithDependency($key, self::$listAuthors, [self::tableName()]);
				}
			}
		}
		return self::$listAuthors;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_author' => 'Id Author',
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'patronymic' => 'Отчество',
        ];
    }

	/**
	 * @return BookStatusesQuery
	 */
	public static function find(){
		return new AuthorsQuery(get_called_class());
	}
}
