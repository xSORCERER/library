<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "book_statuses".
 *
 * @property string $id_book_status
 * @property string $name
 */
class BookStatuses extends \yii\db\ActiveRecord
{
	private static $listStatuses = null;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_statuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 127]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_book_status' => 'Id Book Status',
            'name' => 'Name',
        ];
    }

	/** Получаем все статусы книг одномерным массивом
	 * @return array [id_status => statusName, ...]
	 */
	public static function findAllList(){
		if (self::$listStatuses === null){
			$key = 'all-book-statuses-list';
			self::$listStatuses = Yii::$app->cache->get($key);
			if (self::$listStatuses === false){
				self::$listStatuses = self::find()->select(['id_book_status', 'name'])->asArray()->all();

				if (self::$listStatuses) {
					self::$listStatuses = ArrayHelper::map(self::$listStatuses, 'id_book_status', 'name');
					yii::$app->cache->setWithDependency($key, self::$listStatuses, [self::tableName()]);
				}
			}
		}
		return self::$listStatuses;
	}

	/**
	 * @return BookStatusesQuery
	 */
	public static function find(){
		return new BookStatusesQuery(get_called_class());
	}
}
