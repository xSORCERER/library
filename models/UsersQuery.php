<?php

namespace app\models;

use app\components\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Users]].
 *
 * @see Users
 */
class UsersQuery extends ActiveQuery
{

	public function getDefaultSelect(){
		return array_merge(['id_user', 'firstname', 'lastname', 'email']);
	}

	public function selectAllFields() {
		$this->select(['id_user', 'id_city', 'firstname', 'lastname', 'email']);
		return $this;
	}

    /**
     * @inheritdoc
     * @return Users[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Users|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

	public function getArrayUser($id){
		return $this->cache()->where(['id_user' => (int)$id])->select($this->getDefaultSelect())->one();
	}

	public function getUser($id){
		return $this->cache()->where(['id_user' => (int)$id])->selectAllFields()->one();
	}
}