<?php
/*Created by Edik (16.10.2015 21:55)*/
/**
 * @var \yii\web\View $this
 * @var $model app\models\Books
 * @var string $nameEn
 * @var array $items
 */
$values = $model->$nameEn ?: [0];
$this->registerJsFile('/js/scriptLoader.js', ['position' => \yii\web\View::POS_HEAD]);
?>
<div class="form-group multi-string" id="js_multiString_<?=$nameEn?>">
	<label class="control-label multi-string__label" for="books-<?=$nameEn?>">Авторы</label>
	<?php $first = true; foreach ($values as $value): ?>
		<div class="multi-string__row js_msRow">
			<select id="books-<?=$nameEn?>" class="form-control multi-string__row__select" name="<?=$model->formName()?>[<?=$nameEn?>][]">
				<?php foreach ($items as $k => $val): ?>
					<option <?=(string)$value === (string)$k ? 'selected' : ''?> value="<?= $k ?>"><?= $val ?></option>
				<?php endforeach; ?>
			</select>
			<span class="js_msButtonsContainer">
				<?php if ($first): $first = false;?>
					<a class="multi-string__row__icon js_msButtonAdd"><img src="/img/icon_plus.png"></a>
			    <?php else: ?>
					<a class="multi-string__row__icon js_msButtonDelete"><img src="/img/icon_minus.png"></a>
				<?php endif; ?>
			</span>
		</div>
	<?php endforeach; ?>
	<script type="text/javascript">
		scriptLoader('/js/multiString.js').callFunction(function(){
			multiString().init({id: 'js_multiString_<?=$nameEn?>'});
		});
	</script>
</div>