<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\UsersReg */

$this->title = 'Регистрация';
?>
<div class="site-reg">
    <h1><?= Html::encode($this->title) ?></h1>


    <?php $form = ActiveForm::begin([
        'id' => 'reg-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

	<?= $form->field($model, 'firstname') ?>

	<?= $form->field($model, 'lastname') ?>

	<?= $form->field($model, 'patronymic') ?>

	<?= $form->field($model, 'email') ?>

	<?= $form->field($model, 'password')->passwordInput() ?>

	<?= $form->field($model, 'passwordRepeat')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'reg-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
