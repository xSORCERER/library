<?php
/**
 *  @var app\models\BooksSearch $model
 */
use yii\helpers\Html;

?>

<div class="book__header">
	<h3 class="book__header__h"><?=$model->name; ?></h3>
	<div class="book__header__status book__header__status_id-status_<?=$model->id_status?>"><?= $model->getStatusName(); ?></div>
</div>

<div class="book__authors">
	<?php foreach ($model->bookAuthors as $author): ?>
		<span class="book__authors__name"><?=$author->firstname?> <?=$author->lastname?> <?=$author->patronymic?></span>
	<?php endforeach; ?>
</div>

<div class="book__desc">
	<?= $model->description ?>
</div>

<div class="book__bottom">
	<div class="book__bottom__buttons">
		<?php if ($model->id_status == 1): ?>
			<?php if (\Yii::$app->user->isGuest): ?>
			    Что бы взять книгу нужно <?= Html::a('авторизоваться', ['login']) ?>
			<?php else: ?>
				<button class="btn-disabled" onclick="books.getBook('/ajax/get-book', <?=$model->id_book?>, this)">Взять книгу</button>
			<?php endif; ?>
		<?php else: ?>
			<button class="btn-disabled" disabled>Книга выдана</button>
		<?php endif; ?>
	</div>
</div>