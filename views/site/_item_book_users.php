<?php
/**
 *  @var array $model
 */
?>


<h4><?=$model['name']; ?></h4>

<div class="issued-book__content">
	<span class="issued-book__content__user">
		<span class="issued-book__content__user__name"><?=$model['firstname']; ?></span> взял книгу <?=$model['date_add']; ?>
	</span>

		<button class="issued-book__content__button btn-disabled" onclick="books.returnBook('/ajax/return-book', <?=$model['id_book']?>, this)">
			<?php if ($model['id_user'] == \Yii::$app->user->id): ?>
				Вернуть книгу
			<?php elseif (\Yii::$app->user->can('admin')): ?>
				Книгу вернули
			<?php endif; ?>
		</button>
</div>
