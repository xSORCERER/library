<?php
/* @var $this yii\web\View */
/* @var yii\data\ActiveDataProvider $dataProvider */
use yii\widgets\ListView;

$this->title = 'Библиотека';
?>
<div class="site-index">

    <h1><?=$this->title?></h1>

    <div class="books-count">
	    Всего книг: <span class="books-count__number"><?= $dataProvider->totalCount ?></span>
    </div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'emptyText' => '',
        'itemView' => '_item_index',
        'options' => ['class' => 'books'],
        'itemOptions' => ['class' => 'book']
    ]); ?>
</div>
