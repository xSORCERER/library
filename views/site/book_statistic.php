<?php
/* @var $this yii\web\View */
/* @var array $statistic */

$this->title = 'Статистика';
?>
<div class="site-index">

	<div>
		<div>
			<h3>Статистика по книгам</h3>
			<?php $allListStatuses = \app\models\BookStatuses::findAllList() ?>
			<?php foreach ($statistic['countBooks'] as $idStatus => $count): ?>
			    <?= $allListStatuses[$idStatus] ?>: <?=$count?> <br>
			<?php endforeach; ?>
		</div>
		<div>
			<h3>Статистика по должникам</h3>
			Количество должников: <?=$statistic['countUsers']?>
		</div>
	</div>

</div>
