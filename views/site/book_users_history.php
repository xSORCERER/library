<?php
/* @var $this yii\web\View */
/* @var yii\data\ActiveDataProvider $dataProvider */
use yii\widgets\ListView;

$this->title = 'История выдачи';
?>
<div class="site-index">

    <h1><?=$this->title?></h1>

    <div class="books-count">
	    Всего позиций: <span class="books-count__number"><?= $dataProvider->totalCount ?></span>
    </div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'emptyText' => '',
        'itemView' => '_item_book_users_history',
        'itemOptions' => ['class' => 'issued-book']
    ]); ?>
</div>
