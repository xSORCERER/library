<?php
/**
 *  @var app\models\BookUsersHistory $model
 */
?>


<h4><?= $model->book->name ?></h4>
<div>
	<?php if ($model->user): ?>
		Выдано: <?= $model->user->firstname ?>
	<?php else: ?>
		Пользователь удален
	<?php endif; ?>
</div>
<div>
	Дата выдачи: <?= $model->date_add ?>
</div>
<div>
	Дата возврата: <?= $model->date_return ?>
</div>
