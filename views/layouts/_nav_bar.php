<?php
/*Created by Edik (14.10.2015 14:38)*/
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>

<?php
    NavBar::begin([
        'brandLabel' => 'Библиотека',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-top navbar-inverse navbar-relative-top',
        ],
    ]);
?>

<?php if (Yii::$app->user->isGuest): ?>
    <?=
        Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Вход', 'url' => ['/site/login']],
                ['label' => 'Регистрация', 'url' => ['/site/reg']]
            ],
        ]);
    ?>
<?php else: $isAdmin = Yii::$app->user->can('admin'); ?>
	<?=
	    Nav::widget([
	        'options' => ['class' => 'navbar-nav navbar-left'],
	        'items' => [
		        ['label' => 'Выданные книги', 'url' => ['/site/book-users']],
		        ['label' => 'История выдачи', 'url' => ['/site/history'], 'visible' => $isAdmin],
		        ['label' => 'Статистика', 'url' => ['/site/statistic'], 'visible' => $isAdmin]
	        ],
	    ]);
	?>

    <?=
        Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
	            ['label' => 'Админка', 'url' => ['/admin'], 'active' => $this->context->module->id === 'admin', 'visible' => $isAdmin],
                ['label' => 'Выход (' . Yii::$app->user->identity->firstname . ')', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
            ],
        ]);
    ?>
<?php endif; ?>

<?php
    NavBar::end();
?>