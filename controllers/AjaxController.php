<?php

namespace app\controllers;

use app\models\BookUsers;
use app\models\BookUsersHistory;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class AjaxController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['get-book', 'return-book'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
	                'get-book' => ['post'],
	                'return-book' => ['post']
                ]
            ]
        ];
    }

	//ajax запрос с главной страницы при нажатии на кнопку "взять книгу"
	public function actionGetBook()
	{
		$idBook = Yii::$app->request->post('id_book');
		if ($idBook){
			$model = new BookUsers();
			$model->load(['id_user' => Yii::$app->user->id, 'id_book' => $idBook], '');
			$result = $model->save(false);
		} else {
			$result = false;
		}

		return json_encode(['result' => $result]);
	}

	//ajax запрос со страницы выданных книг при нажатии на кнопку "вернуть книгу"
	public function actionReturnBook(){
		$idBook = Yii::$app->request->post('id_book');
		if ($idBook){
			$model = BookUsers::findOne(['id_book' => $idBook]);

			$newModel = new BookUsersHistory();
			$newModel->load(['id_user' => $model->id_user, 'id_book' => $idBook, 'date_add' => $model->date_add], '');
			$result = $newModel->save(false);

			if ($result) $model->delete();
		} else {
			$result = false;
		}

		return json_encode(['result' => $result]);
	}
}
