<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\UsersLogin;
use app\models\UsersReg;
use app\models\BooksSearch;
use app\models\BooksSearchUsers;
use app\models\BooksSearchHistory;
use app\models\BooksSearchStatistic;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['reg', 'login', 'logout', 'book-users', 'history', 'satistic'],
                'rules' => [
	                [//статистику и испорию смотреть может только admin
                        'actions' => ['satistic', 'history'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [//страница должников доступна только авторизованным
                        'actions' => ['book-users', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['reg', 'login'],
                        'allow' => true,
                        'roles' => ['?']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ]
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

	/*Страница книг*/
	public function actionIndex() {
		$dataProvider = (new BooksSearch())->search();

		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	/*Страница должников*/
	public function actionBookUsers() {
		$dataProvider = (new BooksSearchUsers())->search();

		return $this->render('book_users', [
			'dataProvider' => $dataProvider,
		]);
	}

	/*Страница истории выдачи книг*/
	public function actionHistory() {
		$dataProvider = (new BooksSearchHistory())->search();

		return $this->render('book_users_history', [
			'dataProvider' => $dataProvider,
		]);
	}

	/*Страница статистики*/
	public function actionStatistic() {
		$statisticArray = (new BooksSearchStatistic())->search();

		return $this->render('book_statistic', [
			'statistic' => $statisticArray,
		]);
	}

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new UsersLogin();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	public function actionReg() {
		$model = new UsersReg();

		if ($model->load(\Yii::$app->request->post()) && $model->registration()) {
			return $this->goHome();
		} else {
			return $this->render('reg', [
				'model' => $model,
			]);
		}
	}
}
