<?php
/*Created by Edik (04.07.2015 12:42)*/
namespace app\components\caching;

use yii;
use yii\caching\TagDependency;

class MemCache extends \yii\caching\MemCache{

	function setWithDependency($key, $value, $tags, $duration = 86400){
		$this->set($key, $value, $duration, new TagDependency(['tags' => (array)$tags]));
	}

//	function get($key){
//		return false;
//	}

	public function invalidateTags($tags){
		TagDependency::invalidate(yii::$app->cache, (array)$tags);
	}
}