(function() {
	multiString = function() {
		return new _MultiString();
	};

	function _MultiString() {}
	_MultiString.prototype = {
		init: function(options){
			this.setConst(options);
			this.setEvents();
		},
		appendRow: function() {
			if (this.currentCount < this.maxCount) {
				var $row = this.$container.children('.js_msRow:first').clone().css('display', 'none');
				$row.children('.js_msButtonsContainer').empty().append(this.getButtonDelete());
				$row.find('select option:selected').removeAttr('selected');
				$row.insertAfter(this.$container.children('.js_msRow:last')).slideDown(160);
				if ((++this.currentCount) == this.maxCount) this.buttonAdd.style.display = 'none';
			}
		},
		deleteRow: function(buttonDelete) {
			$(buttonDelete).closest('.js_msRow').slideUp(160, function(){this.parentNode.removeChild(this)});
			this.currentCount--;
			if ((this.currentCount + 1) == this.maxCount) this.buttonAdd.style.display = '';
		},
		getButtonDelete: function() {
			var button = document.createElement('img');
			button.className = 'multi-string__row__icon js_msButtonDelete';
			button.src = '/img/icon_minus.png';
			return button;
		},
		setConst: function(options) {
			this.$container = $('#' + options.id);
			this.maxCount = options.maxCount || 5;
			this.currentCount = this.$container.children('.js_msRow').length;
			this.buttonAdd = this.$container.find('.js_msButtonAdd:first').get(0);
			if (this.currentCount == this.maxCount) this.buttonAdd.style.display = 'none';
		},
		setEvents: function() {
			var self = this;
			this.buttonAdd.onclick = function() {
				self.appendRow();
			};
			this.$container.on('click', '.js_msButtonDelete', function(){
				self.deleteRow(this);
			});
		}
	}
}());