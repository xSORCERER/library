function g(el){
	return typeof el === 'object' ? el : document.getElementById(el);
}

ajax = function(){

	function prepareOptions(options){
		if (!options['type']) options['type'] = 'post';
		if (!options['error']) options['error'] = function(a, b, c){
			if (console) console.log(a, b, c);
		};

		return options;
	}

	function requestOptional(options){
		return $.ajax(prepareOptions(options));
	}

	function simpleRequest(url, data){
		return requestOptional({
			url : url,
			data : data
		});
	}

	return {
		requestOptional : requestOptional,
		simpleRequest: simpleRequest
	}
}();

books = function(){

	function getBook(url, idBook, button){
		button.disabled = true;
		ajax.simpleRequest(url, {id_book: idBook}).then(function(res){
			res = JSON.parse(res);
			if (res.result){
				button.innerHTML = 'Книга выдана';
				$(button).closest('.book').find('.book__header__status:first').removeClass('book__header__status_id-status_1').addClass('book__header__status_id-status_2').text('Выдана');
			} else {
				button.disabled = false;
			}
		});
	}

	function returnBook(url, idBook, button){
		var $issuedBook = $(button).closest('.issued-book');

		$issuedBook.animate({opacity: 0.5}, 300);
		button.disabled = true;
		ajax.simpleRequest(url, {id_book: idBook}).then(function(res){
			res = JSON.parse(res);
			if (res.result){
				button.innerHTML = 'Книга выдана';
				$issuedBook.find('.issued-book__content__button:first').replaceWith('<div>Книга возвращена ' + getDateStr() + '</div>');
			} else {
				button.disabled = false;
			}
		});
	}

	function getDateStr(){
		var date = new Date();
		return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	}

	return {
		getBook: getBook,
		returnBook: returnBook
	};
}();