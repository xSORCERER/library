<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
	    'authManager' => [
			'class' => 'yii\rbac\DbManager'
		],
	    'urlManager' => [
            'class'=>'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false
        ],
        'request' => [
            'cookieValidationKey' => 'uE4Zfzw0z2tHphnvtC6Q3jE99ZJkUUAl',
        ],
        'cache' => [
            'class' => 'app\components\caching\MemCache',
        ],
	    'user' => [
	        'identityClass' => 'app\models\User',
	        'enableAutoLogin' => true
	    ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => require(__DIR__ . '/modules.php'),
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
